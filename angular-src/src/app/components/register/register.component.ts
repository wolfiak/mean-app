import { Component, OnInit } from '@angular/core';
import {ValidateService} from '../../services/validate.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {AuthService} from'../../services/auth.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  name:String;
  username: String;
  email: String;
  password: String;
  constructor(private vs: ValidateService, private fs :FlashMessagesService, private as: AuthService,
    private router: Router) { }

  ngOnInit() {
  }

  onRegisterSubmit(){
    console.log(this.name);
     const user={
       name: this.name,
       email: this.email,
       username: this.username,
       password: this.password
     }
     if(!this.vs.validateRegister(user)){
       this.fs.show('Musisz wporwadzic wszystkie pola',{cssClass:'alert-danger',timeout:100000 });
       return false;
     }
     if(!this.vs.validateEmail(user.email)){
         this.fs.show('Email musi byc prawidlowy',{cssClass:'alert-danger',timeout:3000 });
       return false;
     }

     this.as.registerUser(user).subscribe(data=>{
       if(data.success){
          this.fs.show('Jestes zarejestrowany',{cssClass:'alert-success',timeout:3000 });
          this.router.navigate(['/login']);
       }else{
          this.fs.show('Cos poszlo nie tak',{cssClass:'alert-danger',timeout:3000 });
          console.log("POSZEDL ELSE");
          this.router.navigate(['/register']);
       }
     });
     return true;
  }

}
