const express=require('express');
const router=express.Router();
const passport=require('passport');
const User=require('../models/user');
const jwt=require('jsonwebtoken');
const config=require('../config/database');
router.post('/register',(req,res)=>{
   let newUser= new User({
     name: req.body.name,
     email: req.body.email,
     username: req.body.username,
     password: req.body.password
   });
   User.addUser(newUser, (error, user)=>{
     if(error){
       res.json({success: false, msg:'Field'});
     }else{
       res.json({success: true,msg: 'Dziala'});
     }
   });

});
router.post('/authenticate',(req,res)=>{
  const username=req.body.username;
  const password=req.body.password;
  User.getUserByUsername(username,(err,user)=>{
    if(err){
      throw err;
    }
    if(!user){
      return res.json({success: false,msg:'User not found'})
    }

    User.comparePassword(password,user.password,(err,isMatch)=>{
      if(err){
        throw err;
      }
      if(isMatch){
        const token=jwt.sign(user,config.secret,{
          expiresIn: 604800

        });
          res.json({
            success: true,
            token: 'JWT '+token,
            user:{
              id: user._id,
              name: user.name,
              username: user.username,
              emial: user.email
            }
          });
      }else{
        return res.json({success: false, msg:'Zle haslo'});
      }
    });
  });
});
router.get('/authenticate',(req,res)=>{
  res.send(`AUTHENTICATE`);
});
router.get('/profile',passport.authenticate('jwt',{session:false}),(req,res)=>{
  res.json({user: req.user});
});

module.exports=router;
